# 100-simple-process-mass-balance-app-templates

This is just me keeping my skills in  check while lookin for employment. 
The purpose is to build an app for calculating simple mass and energy balances with publicly available data.
All data rights reserved by owners. I found it on the internet. :-)


Introduction
============

Background
----------
This project is a set of components that can assist to build a simple mass and energy balance
for a metallurgical process.

The idea is to continue utilising my skillset while I look for employment.
It is also a place to showcase the skills if asked in the future by potential employers or just
as a conversation starter in circles of like-minded people in my field.


Components
==========

The components of this app includes the following:

[//]: # (Don't worry about the numbering order here, Markdown ordered lists are automatic starting 
with the number on the first item in the list. So for ease of rearranging item order in the list,
all of them are numbered "1." in the code.) 
1. Documentation (PDF)
1. Process flowsheet (PDF drawing)
1. Back end calculators (Python)
1. Thermochemistry data (SQLite database)
1. Mass balance GUI (PyQT5)
