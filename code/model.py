#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The purpose of this module is to 
"""

__author__ = "Nigel Phuthi"
__copyright__ = "Copyright 2021"
__credits__ = ["Nigel Phuthi"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Nigel Phuthi"
__email__ = "nigel.phuthi@gmail.com"
__status__ = "Development"

import yaml

from base_functions import *
from model_functions import *
from thermochem_functions import *
from results_functions import *


def add_ore(settings):
    pass


def add_reductant(settings):
    pass


def add_flux(settings):
    pass


def batch_feed(settings):
    pass


def preheat(settings):
    pass


def smelt(feed, settings):
    pass


def tap(furnace_content, settings):
    pass


def mass_balance(inputs, outputs):
    pass


def energy_balance(inputs, outputs):
    pass


def element_balance(inputs, outputs):
    pass


def main():
    root = Path(__file__).parents[1]
    settings_file_path = root.joinpath("code/global_settings_file.yaml")
    with open(settings_file_path) as f:
        global_settings = yaml.load(f, yaml.FullLoader)
    db_interface = db_cursor(global_settings)

    print(db_interface.lastrowid)


if __name__ == "__main__":
    main()
