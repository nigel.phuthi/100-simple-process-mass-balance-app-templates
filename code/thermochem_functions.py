#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The purpose of this module is to 
"""

__author__ = "Nigel Phuthi"
__copyright__ = "Copyright 2021"
__credits__ = ["Nigel Phuthi"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Nigel Phuthi"
__email__ = "nigel.phuthi@gmail.com"
__status__ = "Development"


def Hf298(compound):
    """ Return enthalpy of formation from the database if present,
        or calculate using Hess' law. """
    pass


def H_T(compound):
    """ Calculate enthalpy at temperature. """
    pass


def Cp(compound):
    """ Calculate specific heat capacity at temperature. """
    pass


def molar_mass(compound):
    """ Molar mass of a compound. """
    pass


def dHT(compound, T):
    """ Enthalpy at temperature. """
    pass


def dHr(inputs, outputs):
    """ Hess' Law. """
    pass


if __name__ == "__main__":
    pass
