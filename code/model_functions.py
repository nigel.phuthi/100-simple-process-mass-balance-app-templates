#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The purpose of this module is to store common back-end functions used in
     the model. The functions help with calculations, file and data manipulation,
     and results processing (arranging and visualising).
"""

__author__ = "Nigel Phuthi"
__copyright__ = "Copyright 2021"
__credits__ = ["Nigel Phuthi"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Nigel Phuthi"
__email__ = "nigel.phuthi@gmail.com"
__status__ = "Development"


if __name__ == "__main__":
    pass
