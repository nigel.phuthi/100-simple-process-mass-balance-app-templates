#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The purpose of this module is to store common back-end functions used in
    file and data loading and dumping.
"""

__author__ = "Nigel Phuthi"
__copyright__ = "Copyright 2021"
__credits__ = ["Nigel Phuthi"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Nigel Phuthi"
__email__ = "nigel.phuthi@gmail.com"
__status__ = "Development"

import yaml
import sqlite3
import pandas as pd
from math import *
from pathlib import Path


def init_db(db_file_path: Path) -> sqlite3.Connection:
    """ Connect to a database if exists (which is in my case),
        otherwise create a new one given the path.

        :param db_file_path: The location of the sqlite database file on the hard disk. I suppose you can
        add a network location for your purposes if you'd like. I was not interested in that in this
        iteration.

        :returns: An sqlite3 Connection to a database.
    """
    if db_file_path.exists():
        connection = sqlite3.connect(db_file_path)
    else:
        connection = sqlite3.Connection(db_file_path)
    return connection


def db_cursor(settings: dict) -> sqlite3.Connection.cursor:
    """ Get a cursor to interface with the database.

        :param settings: A dictionary containing settings that can be used globally.

        :returns: A cursor to connect to our database.
    """
    return init_db(Path(settings['database']['db_file_path'])).cursor()


def load_settings(settings_file_path: Path) -> dict:
    """ Load settings from a file.

        :param settings_file_path: The location of the settings file passed as pathlib.Path object.
            This is a yaml file in this iteration for simplicity. In a next iteration, I will probably use XML,
            since it is said to have better type and structure controls through its schema than YAML.

        :returns Settings contained in a Python dictionary.
    """
    with open(settings_file_path) as f:
        return yaml.load(f, yaml.FullLoader)


if __name__ == "__main__":
    pass
