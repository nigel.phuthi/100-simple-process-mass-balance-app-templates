Code Resources
==============
1. Base functions: File and data loading and dumping.
1. Calculation functions: Thermochemical calculations only.
1. GUI functions: Functions to build and run the GUI of this app.
1. Database: SQLite store for thermochemical data, results and...

Tex Resources
=============
1. Process flow sheet: Easy visualisation
1. Documentation: Detailed descriptions of model and methodology
1. 
